---
title: Martin Vuk
subtitle: 
comments: false
---

Teaching assistant at [FRI](https://www.fri.uni-lj.si/en/employees/martin-vuk) and cofounder of [Agilicity](https://modelur.eu/).

### I love

- rock climbing, cycling and outdoor sports
- mathematics
- programming

### Contact

```plain
  phone: +386 1 479 8258
  email: mrcin.vuk@gmail.com
  office: R3.26
  
  address:

   Martin Vuk
   Univerza v Ljubljani
   Fakulteta za računalništvo in informatiko
   Večna pot 113
   1000 Ljubljana
```
