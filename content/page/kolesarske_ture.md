## Gorenjska

### Soriška Planina - Gorjuše - Dražgoše

http://bit.ly/32OqFp4

### Okrog Porezna

http://bit.ly/32VqmsK

### Na Dražgoše in Sv. Lenart

http://bit.ly/330vUSX

## Primorska

### Iz Logatca na Kras in nazaj

http://bit.ly/32TfgV4

### Iz Logatca v Vipavsko dolino in na Goro

http://bit.ly/32UXiS7

## Dolenjska

### Od Stične do Radeč in nazaj

https://bit.ly/32sAvO1

## Južna Slovenija

### Veliki Osolnik

http://bit.ly/2YH3UEy

### Iz Ljubljane do Cerknice in nazaj

http://bit.ly/32UYvZD

### Od Ribnice do Kolpe in nazaj

http://bit.ly/32ZZEiP

### Od Kočevja ob Kolpi

http://bit.ly/32XZOqy

### Risnjak (Hrvaška)

http://bit.ly/32Op82i