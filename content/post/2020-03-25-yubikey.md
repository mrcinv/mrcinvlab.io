---
TITLE: Yubikey
DATE: 2020-03-25
PUBLISHDATE: 2020-03-25
DRAFT: true
TAGS: [gesla, varnost, gpg]
---
# Yubikey in življenje brez gesel (skoraj)
Ta opis sloni na [DrDuhov vodič](https://github.com/drduh/YubiKey-Guide)

## SSH

```console
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
```

## Firefox

Izbereš  *Nastavitve -> Zasebnost in varnost -> Varnostne naprave -> Naloži*
in dodaš lokacijo `opensc-pkcs11` knjižnice ( na ubuntuju je to 
`/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so`)

![Nastavi Opensc kot varnostno napravo](opensc-firefox.png)

Nato se ključ pojavi med potrdili uporabnika.

