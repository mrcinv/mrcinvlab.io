---
title: Čez Pavličevo sedlo in Črnivec
subtitle: Najvišji prelazi Kamniško Savinjskih Alp
tags: ["kolo", "krožna tura", "klasična tura"]
weight: 2
---

Navišji prelaz čez Kamniško Savinjske Alpe je [Pavličevo sedlo](https://sl.wikipedia.org/wiki/Pavli%C4%8Devo_sedlo). Krožna pot pelje iz Kamnika Čez Črnivec in [Volovljek](https://sl.wikipedia.org/wiki/Volovljek_(prelaz)) v zgornjo Savinjsko dolino. Čez Pavličevo sedlo, nas cesta pripelje na Koroško v Avstijo in naprej čez Jezersko sedlo na Jezersko. Po dolini Kokre se nato v dolgem spustu vrnemo v Ljubljansko kotlino in Kamnik.

<!--more-->

## Trasa

http://bit.ly/32Wac25