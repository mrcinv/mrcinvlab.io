---
title: Čez Vršič in Predel
subtitle: Kraljevska tura čez najvišji prelaz
tags: ["kolo", "krožna tura"]
weight: 1
---

[Prelaz Vršič](https://sl.wikipedia.org/wiki/Vr%C5%A1i%C4%8D) je s 1611 m.n.v. najvišji cestni prelaz v Sloveniji. Če želimo Vršič vključiti v krožno turo, moramo še splezati še na en prelaz bodisi [Predel](https://sl.wikipedia.org/wiki/Predel) na zahodu ali pa [Soriško planino](https://sl.wikipedia.org/wiki/Sori%C5%A1ka_planina) na vzhodu.

<!--more-->

## Trasa

http://bit.ly/32W7LMZ