---
title: Krožne kolesarke ture
subtitle: Predlogi krožnih kolesarskih tur v bližini
tags: ["kolo"]
date: 2019-07-28
---
Po Sloveniji in v bližini je veliko lepih kolesarskih tur. Tu so zbrani predlogi nekaterih tur dolgih okrog *100 km*, ki so *krožne*, *po asfaltu* in, če je le mogoče, po *cestah z malo prometa*.

<!--more-->
## Gorenjska

### Soriška Planina - Gorjuše - Dražgoše

http://bit.ly/32OqFp4

### Okrog Porezna

http://bit.ly/32VqmsK

### Na Dražgoše in Sv. Lenart

http://bit.ly/330vUSX

## Primorska

### Iz Logatca na Kras in nazaj

http://bit.ly/32TfgV4

### Iz Logatca v Vipavsko dolino in na Goro

http://bit.ly/32UXiS7


## Južna Slovenija

### Veliki Osolnik

http://bit.ly/2YH3UEy

### Iz Ljubljane do Cerknice in nazaj

http://bit.ly/32UYvZD

### Od Ribnice do Kolpe in nazaj

http://bit.ly/32ZZEiP

### Od Kočevja ob Kolpi

http://bit.ly/32XZOqy

### Risnjak (Hrvaška)

http://bit.ly/32Op82i

## Koroška

### Okrog Uršlje gore

https://bit.ly/34coQFx
